PREFIX = /usr/local/nagios
LIBEXECDIR = $(PREFIX)/libexec/
SCRIPTS = check_cups check_glsa check_saned check_lpd check_hddtemp \
	check_link_status check_true check_lysrdiff check_syslog \
	check_ping check_enodia_monitored check_hostextinfo \
	check_hydra check_datorhandbok check_no_server check_iostatE \
	check_nfs_server check_svcs check_zfs check_postgrey check_apt

SUBDIRS = bin rules site-rules

all:;

install:
	mkdir -p $(DESTDIR)$(LIBEXECDIR)
	for s in $(SCRIPTS); \
	do cp $$s $(DESTDIR)$(LIBEXECDIR)/$$s || exit 1; \
	   chmod 755 $(DESTDIR)$(LIBEXECDIR)/$$s || exit 1; \
	done

	if [ -f src/lyskom/.lastos ] ; \
	then (cd src/lyskom && $(MAKE) install) || exit 1 ; \
	else true; \
	fi

dist:
	base=lysator-nagios-plugins-`date +%Y-%m-%d`; \
	mkdir -p $$base/src/lyskom || exit 1; \
	for i in $(SCRIPTS) \
		Makefile \
		src/lyskom/Makefile \
		src/lyskom/buffer.c \
		src/lyskom/buffer.h \
		src/lyskom/check_lyskom.c \
		src/lyskom/lyskom.c \
		src/lyskom/lyskom.h \
		src/lyskom/notify_lyskom.c; \
	do cp $$i $$base/$$i || exit 1; done; \
	find $(SUBDIRS) \( -name .svn -prune \) -o \( -type d -print \) \
	| sed "s%^%$$base/%" | xargs mkdir || exit 1 ; \
	find $(SUBDIRS) \( -name .svn -prune \) -o \( -type f -print \) \
	| sed -e '/\~$$/d' -e '/#$$/d' \
	| while read f ; do cp $$f $$base/$$f || exit 1; done || exit 1; \
	tar cfz $$base.tar.gz $$base || exit 1; \
	rm -r $$base

