# Install the service.
getent services nrpe >/dev/null
if [ $? = 2 ]
then
    echo 'nrpe 5666/tcp # Nagios NRPE' >> /etc/services
fi

# Install the SVC
if grep "^nrpe stream tcp" /etc/inetd.conf >/dev/null 2>&1
then :
else
    echo "nrpe stream tcp nowait nagios $prefix/bin/nrpe nrpe -c $prefix/etc/nrpe.cfg -i" >> /etc/inetd.conf
    pkill -HUP -P 1 -u root -x inetd
fi
