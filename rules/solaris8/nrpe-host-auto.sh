FILESYSTEMS=`get_tempfile nrpfs`
trap "rm -f $FILESYSTEMS" 0

# Create a host-specific config file.
{ df -F ufs; df -F tmpfs; } \
| awk '{ print $1 }' \
| tee $FILESYSTEMS \
| while read mp
  do
    dev=`mount|awk '$1 == "'$mp'" { print $3 }'`
    case $dev in
	/*) arg=$dev;;
	swap) arg=$mp;;
	*) echo unknown: mp=$mp dev=$dev >&2
	   arg=$mp;;
    esac
    echo $arg `echo $mp | sed -e 's%^/$%/root%' -e 's%/%-%g'`
  done \
|awk '{ print "command[check-disk" $2 "]='$prefix'/libexec/check_disk " $1 }'\
> $prefix/etc/nrpe-host-auto.cfg
