# Install the service.
getent services nrpe >/dev/null
if [ $? = 2 ]
then
    echo 'nrpe 5666/tcp # Nagios NRPE' >> /etc/services
fi

# Install the SVC
if svcs svc:/network/nrpe/tcp >/dev/null 2>&1
then :
else
    NRPECFG=`get_tempfile nrpsv`
    echo "nrpe stream tcp nowait nagios $prefix/bin/nrpe $prefix/bin/nrpe -c $prefix/etc/nrpe.cfg --inetd" > $NRPECFG
    inetconv -i $NRPECFG
    rm $NRPECFG
    inetadm -m svc:/network/nrpe/tcp tcp_wrappers=TRUE
fi
