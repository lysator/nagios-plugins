FILESYSTEMS=`get_tempfile nrpfs`
ZPOOLS=`get_tempfile nrzfs`
trap "rm -f $FILESYSTEMS $ZPOOLS" 0

# Create a host-specific config file.
{ df -F ufs; df -F tmpfs; } \
| awk '{ print $1 }' \
| tee $FILESYSTEMS \
| while read mp
  do
    dev=`mount|awk '$1 == "'$mp'" { print $3 }'`
    case $dev in
	/*) arg=$dev;;
	swap) arg=$mp;;
	*) echo unknown: mp=$mp dev=$dev >&2
	   arg=$mp;;
    esac
    echo $arg `echo $mp | sed -e 's%^/$%/root%' -e 's%/%-%g'`
  done \
|awk '{ print "command[check-disk" $2 "]='$prefix'/libexec/check_disk " $1 }'\
> $prefix/etc/nrpe-host-auto.cfg

# FIXME: We need a check_zpool command, that checks the status of the pool,
# and the capacity of the root zfs of that pool, and maybe also recursively 
# checks all child zfs.
if [ -f /usr/sbin/zpool ] && [ -L /dev/zfs ]
then
    /usr/sbin/zpool list -H -o name \
    | grep -v 'no pools available' \
    | tee $ZPOOLS \
    | awk '{ 
        print "command[check-zfs-" $1 "]='$prefix'/libexec/check_zfs " $1 
      }' \
    >> $prefix/etc/nrpe-host-auto.cfg
else
    : > $ZPOOLS
fi
