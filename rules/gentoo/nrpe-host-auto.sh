FILESYSTEMS=`get_tempfile nrpfs`
trap "rm -f $FILESYSTEMS" 0

> $prefix/etc/nrpe-host-auto.cfg
awk < /etc/fstab \
    '! ($1 ~ /\// || $1 ~ /LABEL=/) { next } 
     $1 ~ /^#/ { next }
     $3 == "swap" || $3 == "nfs" { next } 
     $4 ~ /noauto/ { next } 
     $1 ~ /LABEL=/ { print $2, $2; next }
     { print $1, $2 }' \
| while read dev mp
  do
    mpdash=`echo $mp | sed -e 's%^/$%/root%' -e 's%/%-%g'`
    echo "command[check-disk$mpdash]=/usr/nagios/libexec/check_disk -w 10% -c 5% $dev" \
	>> $prefix/etc/nrpe-host-auto.cfg
    echo $mp
  done > $FILESYSTEMS
