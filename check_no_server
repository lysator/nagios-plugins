#!/usr/bin/env python

import socket
import errno
import sys

def unknown(hostname, msg):
    print "UNKNOWN - %s: %s" % (hostname, msg)
    sys.exit(3)

def critical(hostname, msg):
    print "CRITICAL - %s: %s" % (hostname, msg)
    sys.exit(2)

def warning(hostname, msg):
    print "WARNING - %s: %s" % (hostname, msg)
    sys.exit(1)

def ok(hostname, msg):
    print "OK - %s: %s" % (hostname, msg)
    sys.exit(0)

def check_no_server(hostaddr, port, cfg, hostname):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.settimeout(7)
    try:
        s.connect((hostaddr, port))
    except socket.gaierror, err:
        warning(hostaddr, err[1])
    except socket.timeout:
        if cfg.require(hostname):
            critical(hostaddr, "timeout connecting to server")
        else:
            ok(hostaddr, "timeout connecting to server")
    except socket.error, e:
        if e[0] == errno.ECONNREFUSED:
            if cfg.require(hostname):
                critical(hostaddr, e[1])
            else:
                ok(hostaddr, e[1])
        unknown(hostaddr, e[1])

    if cfg.allowed(hostname):
        ok(hostaddr, "connection succeeded")
    else:
        critical(hostaddr, "connection succeeded")

class CfgFile(object):
    def __init__(self, fn=None):
        self.__allowed = {}
        if fn is not None:
            for line in file(fn):
                if line[0] == "#":
                    continue
                cmd, host, comment = line.split(None, 2)
                self.__allowed[host] = cmd

    def allowed(self, host):
        return self.__allowed.get(host) in ["allow", "require"]

    def require(self, host):
        return self.__allowed.get(host) in ["require"]

def usage():
    sys.stderr.write("usage: check_no_server -H host -p port [ -c cfgfile ]\n")
    sys.stderr.write("or: check_no_server [ -c cfgfile ] host port\n")
    sys.exit(1)

def main():
    import getopt

    hostaddr = None
    port = None
    cfg = None
    hostname = None

    opts, args = getopt.getopt(sys.argv[1:], "H:p:c:n:",
                               ["host=", "port=", "cfg=",
                                "name="])
    for opt, val in opts:
        if opt in ("-H", "--host"):
            hostaddr = val
        elif opt in ("-p", "--port"):
            port = int(val)
        elif opt in ("-c", "--cfg"):
            cfg = val
        elif opt in ("-n", "--name"):
            hostname = val

    if hostaddr is None and port is None and len(args) == 2:
        hostaddr = args[0]
        port = int(args[1])

    if hostaddr is None or port is None:
        usage()

    if cfg is not None:
        cfg_data = CfgFile(cfg)
    else:
        cfg_data = CfgFile()

    if hostname is None:
        hostname = hostaddr
    
    check_no_server(hostaddr, port, cfg_data, hostname)

if __name__ == '__main__':
    main()
