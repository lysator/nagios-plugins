if [ `ls -1d ../nrpe-* | wc -l` -eq 1 ] \
    && [ -d ../nrpe-* ] \
    && [ -f ../nrpe-*/nrpe.spec ]
then : 
else
    echo cannot find unique nrpe source in ../nrpe-* >&2
    exit 1
fi
