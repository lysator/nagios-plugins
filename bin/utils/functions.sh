#!/bin/sh

findcfg () {
    cfgdir=$CFGDIR
    file=$1

    if [ -f $cfgdir/$file ]
    then
	echo $cfgdir/$file
	return
    fi

    os=`cat $cfgdir/os`

    while :
    do
      cfgdir=`dirname $cfgdir`
      if [ -z "`echo $cfgdir|grep site-rules`" ]
      then
	  break
      fi

      if [ -f $cfgdir/$file ]
      then
	  echo $cfgdir/$file
	  return
      fi
    done

    if [ -f rules/$os/$file ]
    then
	echo rules/$os/$file
	return
    fi

    if [ -f rules/$file ]
    then
	echo rules/$file
	return
    fi

    return
}

source_fragment () {
    fn=`findcfg $1`
    if [ -z "$fn" ]
    then
	echo Fatal error: fragment $1 not found. >&2
	exit 1
    else
	. ./$fn
    fi
}

run_hook () {
    hook=`findcfg $1`
    if [ -n "$hook" ]
    then
	. ./$hook
    fi
}

contents () {
    fn=`findcfg $1`
    if [ -n "$fn" ]
    then
	cat "$fn"
    fi
}

get_tempfile () {
    mktemp -t $1.XXXXXX 2>/dev/null && return
    tempfile -p $1 2>/dev/null && return
    echo /tmp/$1.$$
}

gentoo_add_use () {
    package="$1"
    use="$2"
    fn=/etc/portage/package.use

    if grep "^$package[ 	]" $fn >/dev/null
    then
	if grep "^$package[ 	].*$use\($\|[ 	]\)" $fn >/dev/null
        then :
	else
	    echo Please add $use use flags to $package in $use. >&2
	    exit 1
	fi
    else
	echo $package $use >> $fn
    fi
}
