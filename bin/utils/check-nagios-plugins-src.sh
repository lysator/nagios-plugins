if [ `ls -1d ../nagios-plugins-* | wc -l` -eq 1 ] \
    && [ -d ../nagios-plugins-* ] \
    && [ -f ../nagios-plugins-*/nagios-plugins.spec ]
then : 
else
    echo cannot find unique nagios-plugins source in ../nagios-plugins-* >&2
    exit 1
fi
