usage () {
    echo $0: usage: $0 cfgdir >&2
}

if [ $# != 1 ]
then
    usage
    exit 1
fi

if [ "x$1" = x-h ] || [ "x$1" = x--help ]
then
    usage
    exit 0
fi

CFGDIR=$1

if [ -d $CFGDIR ]
then :
else
    echo $0: %CFGDIR: not a directory >&2
    exit 1
fi

if [ -f $CFGDIR/os ]
then :
else
    echo $CFGDIR: bad config dir: no "os" file present >&2
    exit 1
fi
