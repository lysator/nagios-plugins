echo
echo BEGIN `hostname | sed 's/\..*//'` nagiosadmin
sed 's/^/fs /' $FILESYSTEMS | sort
sed 's/^/zfs /' $ZPOOLS | sort
echo END
