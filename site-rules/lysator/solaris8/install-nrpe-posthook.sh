#!/bin/sh

# Install gawk locally, as check_iostatE needs it.
if [ -f /opt/lysator/bin/gawk ]
then :
else
    if [ -f /usr/local/bin/gawk ]
    then
        mkdir -p /opt/lysator/bin
        cp /usr/local/bin/gawk /opt/lysator/bin
    else
	echo '*** No gawk found, so not installed in /opt/lysator/bin.' >&2
	echo '*** check_iostatE might fail!' >&2
    fi
fi
