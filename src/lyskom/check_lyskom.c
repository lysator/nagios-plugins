/*
** check_lyskom.c
**
** A simple Nagios plugin to verify LysKOM server status
**
** Copyright (c) 2006 Peter Eriksson <pen@lysator.liu.se>
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <errno.h>
#include <time.h>
#include <math.h>

#include "lyskom.h"

#ifndef CLOCK_HIGHRES
#define CLOCK_HIGHRES CLOCK_REALTIME
#endif


char *host = "130.236.254.15";
int   port = 4894;

char *ident = NULL;
int   user = 0;
char *pass = NULL;

char *argv0 = "check_lyskom";

double time_crit = 2.0; /* 2 seconds */
double time_warn = 1.0; /* 1 second */

double resp_crit = 1.0; /* 1 second */
double resp_warn = 0.1; /* 0.1 seconds */

int clients_crit = 10000;
int clients_warn = 1000;

int rqclients_crit = 10;
int rqclients_warn = 2;

void
error(const char *msg, ...)
{
    va_list ap;

    va_start(ap, msg);
    printf("CRITICAL - ");
    vprintf(msg, ap);
    putchar('\n');
    va_end(ap);
    exit(2);
}


int
time_get(const char *str,
	 double *t)
{
    char c1, c2;
    int rc;


    c1 = c2 = 0;
    
    rc = sscanf(str, "%lf%c%c", t, &c1, &c2);
    if (rc >= 2)
    {
	switch (c1)
	{
	  case 'h':
	    *t *= 60*60;
	    break;
	    
	  case 'm':
	    switch (c2)
	    {
	      case 0:
		*t *= 60;
		break;
	      case 's':
		*t /= 1000.0;
		break;
	      default:
		return -1;
	    }
	    break;

	  case 's':
	    break;

	  case 'u':
	  case '�':
	    *t /= 1000000.0;
	    break;

	  default:
	    return -1;
	}
    }

    if (rc > 0)
	return 0;
    
    return -1;
}


char *
t2unit(double t)
{
    if (t > 60)
	return "m";
    if (t < 0.0001)
	return "�s";
    if (t < 0.1)
	return "ms";
    return "s";
}

double
t2vis(double t)
{
    if (t > 60)
	return t/60.0;
    if (t < 0.0001)
	return t*1000000.0;
    if (t < 0.1)
	return t*1000.0;
    return t;
}

int
main(int argc,
     char *argv[])
{
    int i, rc, clients_c, clients_rq;
    struct tm tmb;
    time_t lyskom_t, our_t;
    double d;
#if 0
    int proto;
    char *ksoft, *kver;
#endif
    struct timespec ts1, ts2;
    double rd;
    

    argv0 = strdup(argv[0]);
    
    for (i = 1; i < argc && argv[i][0] == '-'; i++)
	switch (argv[i][1])
	{
	  case '-':
	    ++i;
	    goto End;

	  case 'H':
	    host = strdup(argv[i]+2);
	    break;

	  case 'P':
	    if (sscanf(argv[i]+2, "%u", &port) != 1)
		error("Invalid argument: %s", argv[i]);
	    break;

	  case 'I':
	    ident = strdup(argv[i]+2);
	    break;

	  case 'u':
	    if (sscanf(argv[i]+2, "%u", &user) != 1)
		error("Invalid argument: %s", argv[i]);
	    break;

	  case 'p':
	    pass = strdup(argv[i]+2);
	    break;

	  case 'c':
	    switch (argv[i][2])
	    {
	      case 'c':
		if (sscanf(argv[i]+3, "%u", &clients_crit) != 1)
		    error("Invalid argument: %s", argv[i]);
		break;
	      case 'w':
		if (sscanf(argv[i]+3, "%u", &clients_warn) != 1)
		    error("Invalid argument: %s", argv[i]);
		break;
	      default:
		    error("Invalid switch: %s", argv[i]);
	    }
	    break;

	  case 'q':
	    switch (argv[i][2])
	    {
	      case 'c':
		if (sscanf(argv[i]+3, "%u", &rqclients_crit) != 1)
		    error("Invalid argument: %s", argv[i]);
		break;
	      case 'w':
		if (sscanf(argv[i]+3, "%u", &rqclients_warn) != 1)
		    error("Invalid argument: %s", argv[i]);
		break;
	      default:
		error("Invalid switch: %s", argv[i]);
	    }
	    break;
	    
	  case 't':
	    switch (argv[i][2])
	    {
	      case 'c':
		if (time_get(argv[i]+3, &time_crit))
		    error("Invalid argument: %s", argv[i]);
		break;
	      case 'w':
		if (time_get(argv[i]+3, &time_warn))
		    error("Invalid argument: %s", argv[i]);
		break;
	      default:
		error("Invalid switch: %s", argv[i]);
	    }
	    break;
	    
	  case 'r':
	    switch (argv[i][2])
	    {
	      case 'c':
		if (time_get(argv[i]+3, &resp_crit))
		    error("Invalid argument: %s", argv[i]);
		break;
	      case 'w':
		if (time_get(argv[i]+3, &resp_warn))
		    error("Invalid argument: %s", argv[i]);
		break;
	      default:
		error("Invalid switch: %s", argv[i]);
	    }
	    break;
	    
	  case 'h':
	    printf("Usage: %s [options]\n", argv[0]);
	    puts("Options:");
	    puts("\t-H<lyskom-server-addr>");
	    puts("\t-P<lyskom-server-port>");
	    puts("\t-u<lyskom-userid>");
	    puts("\t-p<password>");
	    puts("\t-cc<clients-connected-critical-level>");
	    puts("\t-cw<clients-connected-warning-level>");
	    puts("\t-qc<clients-runqueue-critical-level>");
	    puts("\t-qw<clients-runqueue-warning-level>");
	    puts("\t-tc<system-time-offset-critical-level>");
	    puts("\t-tw<system-time-offset-warning-level>");
	    puts("\t-rc<response-time-critical-level>");
	    puts("\t-rw<response-time-warning-level>");
	    return 0;
	    
	  default:
	    error("Invalid switch: %s", argv[i]);
	}

  End:
    if (kom_connect(host, port, ident ? ident : "unknown") != 0)
	error("Connect failed: Host=%s, Port=%u", host, port);
    
    if (user > 0)
    {
	if (kom_login(user, pass ? pass : "") != 0)
	    error("Login failed: User=%u", user);
    }

    clock_gettime(CLOCK_HIGHRES, &ts1);
    rc = kom_time(&tmb);
    clock_gettime(CLOCK_HIGHRES, &ts2);
    if (rc != 0)
	error("Request failed: Server Time: rc=%u", rc);

    lyskom_t = mktime(&tmb);
    time(&our_t);
    d = fabs(difftime(lyskom_t, our_t));

    rd = ((double) (ts2.tv_sec-ts1.tv_sec) +
	  (double)(ts2.tv_nsec-ts1.tv_nsec)/1000000000.0);
    
#if 0
    if (kom_version(&proto, &ksoft, &kver) != 0)
	error("Request failed: Server version");
#endif
    if (kom_clients(&clients_c) != 0)
	error("Request failed: Connected clients");

    if (kom_runqueue(&clients_rq) != 0)
	error("Request failed: Clients in run-queue");

    
    if (d > time_crit)
    {
	printf("CRITICAL - Server time is %f second(s) off\n", d);
	return 2;
    }
    
    if (clients_rq > rqclients_crit)
    {
	printf("CRITICAL - Clients in run-queue: %u\n", clients_rq);
	return 2;
    }
    
    if (clients_c > clients_crit)
    {
	printf("CRITICAL - Connected clients: %u\n", clients_c);
	return 2;
    }
    
    if (rd > resp_crit)
    {
	printf("CRITICAL - Response time is %f %s\n", t2vis(rd), t2unit(rd));
	return 2;
    }

    if (d > time_warn)
    {
	printf("WARNING - Server time is %f second(s) off\n", d);
	return 1;
    }

    if (clients_rq > rqclients_warn)
    {
	printf("WARNING - Clients in run-queue: %u\n", clients_rq);
	return 1;
    }
    
    if (clients_c > clients_warn)
    {
	printf("WARNING - Connected clients: %u\n", clients_c);
	return 1;
    }
    
    if (rd > resp_warn)
    {
	printf("WARNING - Response time is %f %s\n", t2vis(rd), t2unit(rd));
	return 1;
    }

    printf("OK - Resp: %.2f %s, Clients: %u (%u in run-queue), Time: %s",
	   t2vis(rd), t2unit(rd), clients_c, clients_rq, asctime(&tmb));
    return 0;
}
