/*
** lyskom.c
**
** Simple LysKOM communications routines
**
** Copyright (c) 2006 Peter Eriksson <pen@lysator.liu.se>
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <errno.h>
#include <ctype.h>
#include <unistd.h>
#include <time.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "lyskom.h"


static FILE *rfp = NULL;
static FILE *wfp = NULL;
static int rqid = 0;



static int
kom_get_response(FILE *fp,
		 char *buf,
		 size_t bufsize)
{
    int c, p = 0;


    if (bufsize < 1)
	return -1;

    while ((c = getc(fp)) != EOF && isspace(c))
	;
    if (c == EOF)
	return 0;
    
    buf[p++] = c;
    while (p < bufsize-1 && (c = getc(fp)) != EOF && !(c == '\n' || c == '\r'))
	buf[p++] = c;
    buf[p] = '\0';
    return 1;
}


int
kom_connect(const char *host,
	    int port,
	    const char *ident)
{
    struct sockaddr_in sin;
    int fd, rc;
    char buf[1024];
    

    memset(&sin, 0, sizeof(sin));
    sin.sin_family = AF_INET;
    sin.sin_addr.s_addr = inet_addr(host);
    sin.sin_port = htons(port);

    fd = socket(PF_INET, SOCK_STREAM, 0);
    if (fd < 0)
	return -1;
    
    while ((rc = connect(fd, (struct sockaddr *) &sin, sizeof(sin))) < 0 &&
	   errno == EINTR)
	;
    if (rc < 0)
    {
	close(fd);
	return -2;
    }

    rfp = fdopen(fd, "r");
    wfp = fdopen(dup(fd), "w");

    fprintf(wfp, "A %uH%s\r", strlen(ident), ident);
    fflush(wfp);
    if (fgets(buf, sizeof(buf), rfp) == NULL)
    {
	fclose(rfp);
	fclose(wfp);
	return -3;
    }
    
    if (strncmp(buf, "LysKOM", 6) != 0)
    {
	fclose(rfp);
	fclose(wfp);
	return -4;
    }

    return 0;
}


int
kom_request(const char *msg, ...)
{
    char buf[1024];
    va_list ap;
    int id, rc, rs;
    
    
    va_start(ap, msg);
    fprintf(wfp, "%u ", ++rqid);
    vfprintf(wfp, msg, ap);
    putc('\n', wfp);
    fflush(wfp);
    va_end(ap);

    do {
	if (kom_get_response(rfp, buf, sizeof(buf)) < 1)
	    return -1;
    } while (buf[0] == ':'); /* Loop in case of Async Msg */
    
    if (sscanf(buf, "=%u", &id) == 1 && id == rqid)
	return 0;
    
    else if (sscanf(buf, "%%%u %u %u", &id, &rc, &rs) == 3)
	return rc;
    
    return -1;
}

     
int
kom_login(int user,
	  const char *pass)
{
    return kom_request("62 %u %uH%s 1", user, strlen(pass), pass);
}


int
kom_time(struct tm *tp)
{
    char buf[1024];
    int id, rc, rs;
    
    
    fprintf(wfp, "%u ", ++rqid);
    fprintf(wfp, "35");
    putc('\n', wfp);
    fflush(wfp);

    do {
	if (kom_get_response(rfp, buf, sizeof(buf)) < 1)
	    return -1;
    } while (buf[0] == ':'); /* Loop in case of Async Msg */
    
    if (sscanf(buf, "=%u %u %u %u %u %u %u %u %u %u", &id,
	       &tp->tm_sec, &tp->tm_min, &tp->tm_hour, &tp->tm_mday, &tp->tm_mon, &tp->tm_year,
	       &tp->tm_wday, &tp->tm_yday, &tp->tm_isdst) == 10 && id == rqid)
    {
	return 0;
    }
    
    else if (sscanf(buf, "%%%u %u %u", &id, &rc, &rs) == 3)
	return rc;
    
    return -1;
}


char *
kom_geth(const char *buf,
	 int *ip)
{
    char *rp;
    int len;


    while (isspace(buf[*ip]))
	++*ip;

    if (sscanf(buf+*ip, "%u", &len) != 1)
	return NULL;

    while (isdigit(buf[*ip]))
	++*ip;

    if (buf[*ip] != 'H')
	return NULL;

    ++*ip;
    
    rp = malloc(len+1);
    if (!rp)
	return NULL;

    memcpy(rp, buf+*ip, len);
    *ip += len;
    rp[len] = '\0';
    
    return rp;
}


int
kom_version(int *proto,
	    char **stype,
	    char **sver)
{
    char buf[1024];
    int id, rc, rs, i;
    
    
    fprintf(wfp, "%u ", ++rqid);
    fprintf(wfp, "75");
    putc('\n', wfp);
    fflush(wfp);

    do {
	if (kom_get_response(rfp, buf, sizeof(buf)) < 1)
	    return -1;
    } while (buf[0] == ':'); /* Loop in case of Async Msg */
    
    if (sscanf(buf, "=%u %u", &id, proto) == 2 && id == rqid)
    {
	for (i = 1; isdigit(buf[i]); ++i)
	    ;
	for (;isspace(buf[i]); ++i)
	    ;
	for (;isdigit(buf[i]); ++i)
	    ;
	for (;isspace(buf[i]); ++i)
	    ;
	*stype = kom_geth(buf, &i);
	*sver = kom_geth(buf, &i);
	return 0;
    }
    
    else if (sscanf(buf, "%%%u %u %u", &id, &rc, &rs) == 3)
	return rc;
    
    return -1;
}


int
kom_clients(int *clients)
{
    char buf[1024];
    int id, rc, rs, n;
    
    
    fprintf(wfp, "%u ", ++rqid);
    fprintf(wfp, "112 7Hclients");
    putc('\n', wfp);
    fflush(wfp);

    do {
	if (kom_get_response(rfp, buf, sizeof(buf)) < 1)
	    return -1;
    } while (buf[0] == ':'); /* Loop in case of Async Msg */
    
    if (sscanf(buf, "=%u %u { %u", &id, &n, clients) == 3 && id == rqid)
	return 0;
    
    else if (sscanf(buf, "%%%u %u %u", &id, &rc, &rs) == 3)
	return rc;
    
    return -1;
}

int
kom_runqueue(int *clients)
{
    char buf[1024];
    int id, rc, rs, n;
    
    
    fprintf(wfp, "%u ", ++rqid);
    fprintf(wfp, "112 16Hrun-queue-length");
    putc('\n', wfp);
    fflush(wfp);

    do {
	if (kom_get_response(rfp, buf, sizeof(buf)) < 1)
	    return -1;
    } while (buf[0] == ':'); /* Loop in case of Async Msg */
    
    if (sscanf(buf, "=%u %u { %u", &id, &n, clients) == 3 && id == rqid)
	return 0;
    
    else if (sscanf(buf, "%%%u %u %u", &id, &rc, &rs) == 3)
	return rc;
    
    return -1;
}

     



