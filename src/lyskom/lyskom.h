/*
** lyskom.h
**
** Simple LysKOM communications routines
**
** Copyright (c) 2006 Peter Eriksson <pen@lysator.liu.se>
*/

#ifndef LYSKOM_H

#include <time.h>

extern int kom_connect(const char *host,
		       int port,
		       const char *ident);

extern int kom_login(int user,
		     const char *pass);

extern int kom_request(const char *msg, ...);

extern int kom_time(struct tm *tp);

extern int kom_version(int *proto,
			   char **stype,
			   char **sver);

extern int kom_clients(int *clients);

extern int kom_runqueue(int *clients);

#endif
