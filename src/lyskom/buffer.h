/*
** buffer.h
**
** Simple character buffer subroutines
**
** Copyright (c) 2006 Peter Eriksson <pen@lysator.liu.se>
*/

#ifndef BUFFER_H
#define BUFFER_H 1

typedef struct buffer
{
    char *buf;
    int len;
    int size;
} BUFFER;


extern void
buf_init(BUFFER *bp);

extern int
buf_putc(BUFFER *bp,
	 char c);

extern int
buf_puts(BUFFER *bp,
	 const char *s);

extern void
buf_clear(BUFFER *bp);

extern char *
buf_getall(BUFFER *bp);

extern int
buf_save(BUFFER *bp,
	 FILE *fp);

extern int
buf_load(BUFFER *bp,
	 FILE *fp);

extern int
buf_length(BUFFER *bp);

extern int
buf_strip(BUFFER *bp);

#endif
