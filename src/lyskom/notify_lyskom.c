/*
** notify_lyskom.c
**
** A simple tool to send alerts/write messages in a LysKOM database
**
** Copyright (c) 2006 Peter Eriksson <pen@lysator.liu.se>
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <stdarg.h>

#include "lyskom.h"
#include "buffer.h"


char *host = "130.236.254.15";
int   port = 4894;

char *ident = NULL;
int   user = -1;
char *pass = NULL;

char *argv0 = "notify_lyskom";

void
error(const char *msg, ...)
{
    va_list ap;

    va_start(ap, msg);
    fprintf(stderr, "%s: ", argv0);
    vfprintf(stderr, msg, ap);
    putc('\n', stderr);
    va_end(ap);
    exit(1);
}



static void
usage(void)
{
    printf("%s: usage: %s [ options... ] recipients...\n",
	   argv0, argv0);
    printf("Each recipient is a decimal conference number.  The message\n");
    printf("is read from stdin.  Valid options:\n");
    printf("  --       Marks end of options.\n");
    printf("  -H<ip>   IP address of LysKOM server host.\n");
    printf("  -P<port> Port of LysKOM server.\n");
    printf("  -I<id>   Identity to use in LysKOM handshake.\n");
    printf("  -u<pno>  Person number to log in as.\n");
    printf("  -p<pwd>  Password to log in with.\n");
    printf("  -a       Send an async message instead of creating a text.\n");
    printf("  -s<subj> Subject (not used for async messages).\n");
    printf("  -h       Print this help message.\n");
    exit(0);
}
    

int
main(int argc,
     char *argv[])
{
    int async = 0, i, rc, target;
    char *subject = NULL;
    BUFFER buf;

    
    argv0 = strdup(argv[0]);
    
    for (i = 1; i < argc && argv[i][0] == '-'; i++)
	switch (argv[i][1])
	{
	  case '-':
	    ++i;
	    goto End;

	  case 'H':
	    host = strdup(argv[i]+2);
	    break;

	  case 'P':
	    if (sscanf(argv[i]+2, "%u", &port) != 1)
		error("invalid argument: %s", argv[i]);
	    break;

	  case 'I':
	    ident = strdup(argv[i]+2);
	    break;

	  case 'u':
	    if (sscanf(argv[i]+2, "%u", &user) != 1)
		error("invalid argument: %s", argv[i]);
	    break;

	  case 'p':
	    pass = strdup(argv[i]+2);
	    break;

	  case 'a':
	    ++async;
	    break;

	  case 's':
	    subject = strdup(argv[i]+2);
	    break;

	  case 'h':
	    usage();

	  default:
	    error("invalid switch: %s (try -h for help)", argv[i]);
	}

  End:
    buf_init(&buf);
    buf_load(&buf, stdin);
    buf_strip(&buf);

    if (kom_connect(host, port, ident ? ident : "unknown") != 0)
	error("kom_connect: host=%s, port=%u", host, port);

    if (user < 0 || !pass)
	error("missing user/password");
    
    if (kom_login(user, pass) != 0)
	error("kom_login: user=%u", user);

    while (i < argc)
    {
	if (sscanf(argv[i], "%u", &target) != 1)
	    error("invalid target: %s", argv[i]);

	if (async)
	    rc = kom_request("53 %u %uH%s", target, buf_length(&buf), buf_getall(&buf));
	else
	{
	    
	    
	    rc = kom_request("86 %uH%s\n%s 1 { 0 %u } 0 { }",
			strlen(subject ? subject : "")+buf_length(&buf)+1,
			(subject ? subject : ""), buf_getall(&buf), target);
	    
	}

	if (rc != 0)
	    error("kom_request: rc=%u", rc);

	++i;
    }

    return 0;
}
